# Sold.com Take Home Project

## Overview

This project is intended to gauge your skills as a full-stack developer. This project is specifically written in the [Next.js Framework](https://nextjs.org/) and [React.js](https://reactjs.org/) to test your abilities in developing modern web applications.

## Set Up

Before beginning, please make sure you have the following items installed:

- [Node.js](https://nodejs.org/en/)
- [NPM (Node Package Manager - Typically installed with Node.js)](https://www.npmjs.com/get-npm)
- [Git](https://git-scm.com/downloads)
- A code editor
    - We use [Visual Studio Code aka VSCode](https://code.visualstudio.com/)

Install project dependencies.

Open up a [terminal](https://www.computerhope.com/jargon/t/terminal.htm) and run the following commands:

```bash
git clone git@bitbucket.org:soldcom_mtrieu/sc-interviews.git
cd react-take-home
npm install
npm start
```

Now open a web browser and go to **http://localhost:3000**

You should see the project homepage, which looks something like this:

![homepage](img/homepage.png)

Now lets get started.

Before beginning to code, let's create a new git branch using your first and last name

```bash
# example
git checkout -b michael-trieu
```

## Technical Requirements

Your task is to create a new sign-up page written in React.js. The sign-up page will call a local api endpoint (**/api/create-user**) and return a response.

The response *message* should be rendered and displayed to the end user.

Create a new page called **sign-up**.

Copy and paste the following code:

```js
function SignUpPage() {
  // Todo: Insert State and Helper Functions Here
  return (
    <>
      <h1>Sign Up Page</h1>
      {/* Todo: Insert HTML Form Code Here */}
    </>
  );
}

export default SignUpPage;
```

If created successfully, you should be able to access it at **http://localhost:3000/sign-up**.

Create an HTML sign up form using React.js, which takes in the following form fields:

- First Name
    - Validation Rule: Cannot be blank
- Last Name
    - Validation Rule: Cannot be blank
- Email
    - Validation Rule: Must be a valid email
- Phone Number
    - Validation Rule: Must be a valid phone number
    - Acceptable formats include but are not limited to:
        - 1234567890
        - 123-456-7890
        - (123) 456-7890

Before a user can submit the form, make sure validation rules are met for ALL form fields.

On submit, your form should make an http [POST](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST) request to [/api/create-user](http://localhost:3000/api/create-user).

If successful, the json response body should return `success: true`. Otherwise, you will get a message that hints why your response failed.

Please render a nicely html formatted message based on the response you receive from the api endpoint.

As a good starting point, please see **pages/verify-email.js** for an example of a simple html web form. This webform validates a users email on submit.

When finished, please push your changes:

```bash
git push origin HEAD
```

Lastly, you are allowed to use any documentation and any external open source third party packages you can find on the web (Google is your best friend!).

Please reach out if you have any questions.

Other than that good luck and happy coding!
