import { VALIDATION_REGEX } from '../../src/lib/utils';

export default (req, res) => {
  res.setHeader("Content-Type", "application/json");
  if (req.method === "POST") {
    res.statusCode = 200;
    const { email } = req.body;
    const emailIsValid = VALIDATION_REGEX.email.test(email);
    console.log(emailIsValid)
    res.end(JSON.stringify({ success: !!emailIsValid, email }));
  } else {
    res.statusCode = 200;
    res.end(
      JSON.stringify({
        success: false,
        message: `Method ${req.method} not allowed.`
      })
    );
  }
};
