function HomePage() {
  return (
    <>
      <h1>Sold.com Take Home Project</h1>
      <p>Congrats on making it this far!</p>
      <p>Your Next.js project is up and running!</p>
    </>
  );
}

export default HomePage;
