import React from 'react';

function VerifyEmailPage() {
  const [email, setEmail] = React.useState("");
  const [error, setError] = React.useState(null);
  const [isEmailValid, setEmailValid] = React.useState(null);

  const handleEmailChange = (event) => {
    const email = event.target.value;
    setEmail(email);
  }

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    console.log('submitting...', email);
    try {
      const response = await validateEmail(email);
      if (response.ok) {
        const data = await response.json();
        setEmailValid(data.success);
        return;
      }
      throw new Error(`Response returned with status ${response.status}`);
    } catch (e) {
      console.error(e);
      setError(e.message);
    }
  }

  const validateEmail = async (email) => {
    return fetch('/api/verify-email', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email })
    });
  }

  return (
    <>
      <h2>Email Tester</h2>
      <form onSubmit={handleFormSubmit}>
        <input name="email" value={email} onChange={handleEmailChange} />
        <button type="submit">Submit</button>
      </form>
      {isEmailValid && <p style={{ color: 'green' }} >Email is valid!</p>}
      {isEmailValid === false && <p style={{ color: 'red' }} >Email is NOT valid! Please enter a valid email.</p>}
      {error && <p style={{ color: 'red' }}>{error}</p>}
    </>
  )
}

export default VerifyEmailPage;